﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaccineParticleCollision : MonoBehaviour
{
    public ParticleSystem vaccineParticles;

    List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();


    // Start is called before the first frame update
    void Start()
    {
        vaccineParticles = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(vaccineParticles, other, collisionEvents);

        for (int i = 0; i < collisionEvents.Count; i++)
        {
            if(other.transform.tag == "dendritic cell morphing")
            {
                if (!other.GetComponent<morphingDendriticCell>().startMorphing)
                {
                    other.GetComponent<morphingDendriticCell>().startMorphing = true;
                }

                ParticleSystem ps = other.GetComponentInChildren<ParticleSystem>();
                ps.Play();
            }
        }
    }
}
