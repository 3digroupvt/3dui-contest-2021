﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SWS;
using UnityEngine.Playables;

public class StopTimelineAndSplinemove : MonoBehaviour
{

    public Transform touchR;
    public bool inputB;
    public bool isButtonPressed;
    

    public PlayableDirector pDirector;
    public splineMove nanoshipSpline;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        touchR.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        touchR.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);

        
        if (OVRInput.GetDown(OVRInput.RawButton.B))
        {
            isButtonPressed = true;
            inputB = !inputB;
        }
       
        

        if (inputB && isButtonPressed ==true)
        {
            PauseTimelineAndSplinemove();
            isButtonPressed = false;
        }

        if (!inputB && isButtonPressed == true)
        {
            ResumeTimelineAndSplinemove();
            isButtonPressed = false;
        }
    }



    void PauseTimelineAndSplinemove()
    {
        pDirector.Pause();
        nanoshipSpline.Pause();
    }

    void ResumeTimelineAndSplinemove()
    {
        pDirector.Resume();
        nanoshipSpline.Resume(); 
    }
}
