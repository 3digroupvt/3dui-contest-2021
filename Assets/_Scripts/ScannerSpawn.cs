﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ScannerSpawn : MonoBehaviour
{
    private Vector3 initialScale;
    private Material scanMat;
    private float _fadeTime = 3f;
    public static int count = 0;
    // Start is called before the first frame update
    void OnEnable()
    {
        scanMat = GetComponent<Renderer>().material;
        initialScale = transform.localScale;
        StartCoroutine(Scan());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "antibody") {
            if (other.transform.parent.GetChild(1).tag == "dendritic")
            {
                other.GetComponent<Renderer>().material = Resources.Load("FresnelGlow") as Material;
                if (!other.transform.parent.parent.GetComponent<APC_Identifier>().isIdentified)
                {
                    other.transform.parent.GetChild(1).gameObject.SetActive(true);
                    other.transform.parent.parent.GetComponent<APC_Identifier>().isIdentified = true;
                }
                count++;
            }
        }
    }

    IEnumerator Scan() {
        Tween myTween = transform.DOScale(initialScale * 100, _fadeTime);
        scanMat.DOFloat(0f, "Vector1_4F0098DF", _fadeTime);
        yield return myTween.WaitForCompletion();
        Destroy(gameObject);
    }

}
