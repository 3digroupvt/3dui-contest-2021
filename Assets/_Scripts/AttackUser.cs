﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AttackUser : MonoBehaviour
{
    private ParticleSystem ps;
    private GameObject player;
    public enum colorType { blue, green, yellow };
    public GameObject[] antibody;
    public colorType _colorType;
    public List<ParticleCollisionEvent> collisionEvents;
    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, player.transform.position) < 80f)
        {
            transform.parent.DOLookAt(player.transform.position, 1f);
            if(!ps.isPlaying)
            ps.Play();
        }
        else {
            if (ps.isPlaying)
                ps.Stop();
        }
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.transform.tag == "glass")
        {
            Debug.Log("hehe");
            int numCollisionEvents = ps.GetCollisionEvents(other, collisionEvents);
            Vector3 pos = collisionEvents[0].intersection;
            Vector3 normal = collisionEvents[0].normal;
            //If not already there, instantiate the antibody at pos
            if (_colorType == colorType.blue)
            {
                GameObject antibodytemp = Instantiate(antibody[0], pos, Quaternion.identity);
                antibodytemp.transform.SetParent(other.transform);
            }
            else if (_colorType == colorType.green)
            {
                GameObject antibodytemp = Instantiate(antibody[1], pos, Quaternion.identity);
                antibodytemp.transform.SetParent(other.transform);
            } else if (_colorType == colorType.yellow)
            {
                GameObject antibodytemp = Instantiate(antibody[2], pos, Quaternion.identity);
                antibodytemp.transform.SetParent(other.transform);
            }
        }
    }
}
