﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdVIewPOVTrigger : MonoBehaviour
{
    public CameraTransition camTransition;
    public bool is3rdPOV;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Nanobot" && !is3rdPOV)
        {
            
            camTransition.moveToThirdPersonViewRight();
            is3rdPOV = true;
        }

        if(other.tag == "Nanobot" && is3rdPOV)
        {
            //camTransition = other.gameObject.GetComponentInChildren<CameraTransition>();
            camTransition.moveToFirstPersonView();
        }
    }
}
