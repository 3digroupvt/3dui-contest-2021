﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CountdownTimer : MonoBehaviour
{

    public PlayableDirector pDirector;
    public float timer;
    public float resetValue;
    public bool isTimerStarted;

    public AntibodySpray antibodySprayScript;
    public bool resumeTimelineOnce;
    
    // Start is called before the first frame update
    void Start()
    {
        pDirector = GetComponent<PlayableDirector>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimerStarted)
        {
            timer -= Time.deltaTime;
        }

        if(timer <= 0f && pDirector.state == PlayState.Paused)
        {
            isTimerStarted = false;
            pDirector.Resume();
            timer = resetValue;
        }

        if(antibodySprayScript.isTimelineResumed && !resumeTimelineOnce && pDirector.state == PlayState.Paused)
        {
            resumeTimelineOnce = true;
            StartCoroutine(ResumeTimelineOnceAfterAntibodyspray());
        }
    }

    public void StartTimer()
    {
        isTimerStarted = true;
    }

    IEnumerator ResumeTimelineOnceAfterAntibodyspray()
    {
        yield return new WaitForSeconds(3f);

        pDirector.Resume();
    }
}
