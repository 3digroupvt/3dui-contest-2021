﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CheckTimeAtWaypoints : MonoBehaviour
{

    public List<GameObject> wayPoints = new List<GameObject>();
    public List<double> timelineTime = new List<double>();
    //public GameObject[] wayPointsOnSpline;

    public float startTime;
    public PlayableDirector pDirector;
    
    // Start is called before the first frame update
    void Start()
    {
        pDirector.time = startTime;
       // wayPointsOnSpline = GameObject.FindGameObjectsWithTag("Waypoint");
               
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Waypoint")
        {
            wayPoints.Add(other.gameObject);
            timelineTime.Add(pDirector.time);
            Debug.Log(other.gameObject.name + ": " + pDirector.time);
        }
    }
}
