﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectTarget : MonoBehaviour
{
    public int id;
    public int length;
    public Material whiteMat;
    public Material redMat;
    public static bool[] isHit;
    public static bool finalHit;

    // Start is called before the first frame update
    void OnEnable()
    {
        finalHit = false;
        isHit = new bool[length];
        isHit[id] = false;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        for (int i = 0; i < length; i++) {
            if (isHit[i]) {
                finalHit = true;
                break;
            }
            finalHit = false;
        }

        if (finalHit)
        {
            GetComponent<MeshRenderer>().material = whiteMat;
        }
        else {
            GetComponent<MeshRenderer>().material = redMat;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (SlingShot.isShoting && (other.transform.parent.tag == "dendritic") && other.transform.parent.GetComponent<APC_Identifier>().isIdentified)
        {
            isHit[id] = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (SlingShot.isShoting && (other.transform.parent.tag == "dendritic") && other.transform.parent.GetComponent<APC_Identifier>().isIdentified)
        {
            isHit[id] = false;
        }
    }
}
