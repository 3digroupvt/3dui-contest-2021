﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SlingShot : MonoBehaviour
{
    public Animator nanobotAnim;
    public GameObject vaccineParticle;
    public UserControl UC;
    public Transform placeholder;
    public Transform origin;
    public Transform vaccineholder;
    public Transform RTOrigin;
    public Transform RBOrigin;
    public Transform LTOrigin;
    public Transform LBOrigin;
    public Transform RTframeOrigin;
    public Transform RBframeOrigin;
    public Transform LTframeOrigin;
    public Transform LBframeOrigin;
    public Transform shotPoint;
    public Transform controller;
    public Transform trajectory;
    public LineRenderer RTLine;
    public LineRenderer RBLine;
    public LineRenderer LTLine;
    public LineRenderer LBLine;
    public LineRenderer shotLine;
    public float gain;
    public float slinggain;
    public float maxLength;
    public static bool isShoting;
    private bool slingshot;
    public static bool slingshotActive;
    private Vector3 initialPos;
    private GameObject temp;
    private int length;
    private Vector3 gravity = Physics.gravity;
    private Vector3 velocitytemp;
    private int len;
    private Material tempMat;
    public AudioSource slingshotAudio;
    public AudioSource UpAudio;
    public GameObject[] canister;
    public GameObject reticle;
    public Material lowMat;
    public Animator midDisplayAnim;
    public bool slingshotActivated = false;
    // Start is called before the first frame update
    void Start()
    {
        slingshot = false;
        slingshotActive = false;
        length = trajectory.childCount;
        trajectory.gameObject.SetActive(false);
        len = canister.Length;
        tempMat = canister[0].GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LateUpdate()
    {
        //if (UC.isControllerActive)
        //{
        if (OVRInput.GetDown(OVRInput.RawButton.Y) && slingshotActivated) {
            SlingshotToggle();
        }

            if (slingshotActive)
            {
                RTLine.SetPositions(new Vector3[] { RTOrigin.position, RTframeOrigin.position });
                RBLine.SetPositions(new Vector3[] { RBOrigin.position, RBframeOrigin.position });
                LTLine.SetPositions(new Vector3[] { LTOrigin.position, LTframeOrigin.position });
                LBLine.SetPositions(new Vector3[] { LBOrigin.position, LBframeOrigin.position });

            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
             {
                nanobotAnim.enabled = false;

                slingshot = true;
                isShoting = true;
                trajectory.gameObject.SetActive(true);
                shotLine.enabled = true;
                placeholder.position = controller.position;
                temp = Instantiate(vaccineParticle, shotPoint.position, Quaternion.identity);
                temp.transform.SetParent(this.transform);
             }

            if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
            {
                slingshot = false;
                isShoting = false;
                trajectory.gameObject.SetActive(false);
                //temp.GetComponent<Rigidbody>().isKinematic = false;
                if (DetectTarget.finalHit && len > 0)
                {
                    temp.GetComponent<Rigidbody>().useGravity = true;
                    temp.GetComponent<Rigidbody>().velocity = (origin.position - temp.transform.position) * gain;
                    len = len - 1;
                    canister[len].GetComponent<Renderer>().material = lowMat;
                }
                else {
                    Destroy(temp);
                }
                shotLine.enabled = false;
                float distancetemp = Vector3.Distance(transform.position, origin.position);
                StartCoroutine(slingshotShake(distancetemp));
                slingshotAudio.Play();
            }

            if (slingshot)
            {
                initialPos = placeholder.position;
                
                shotLine.SetPositions(new Vector3[] { initialPos, controller.position });

                Vector3 sling = controller.position - initialPos;
                //shotPoint.position = origin.position + sling * gain;
                float slinglength = sling.magnitude;
                shotPoint.position = origin.position + sling.normalized * Mathf.Clamp(slinglength, 0f, maxLength) * slinggain;
                temp.transform.position = shotPoint.transform.position;
                vaccineholder.position = shotPoint.transform.position;
                //origin.position = shotPoint.position;
                velocitytemp = (origin.position - temp.transform.position) * gain;
                for (int i = 0; i < length; i++)
                {
                    trajectory.GetChild(i).position = CalculatePosition(.14f * (i + 1), velocitytemp, shotPoint.transform.position);
                }
            }
        }
        //}
    }

    public void SlingshotOff()
    {
        slingshotActive = false;
        nanobotAnim.enabled = true;
        reticle.SetActive(true);
        nanobotAnim.SetBool("slingshotExpand", false);
        midDisplayAnim.SetBool("slingshotOn", false);
    }
    public void SlingshotToggle() {
        slingshotActive = !slingshotActive;
        if (slingshotActive)
        {
            nanobotAnim.enabled = true;
            nanobotAnim.SetBool("slingshotExpand", true);
            UpAudio.Play();
            reticle.SetActive(false);
            midDisplayAnim.SetBool("slingshotOn", true);
        }
        else
        {
            nanobotAnim.enabled = true;
            reticle.SetActive(true);
            nanobotAnim.SetBool("slingshotExpand", false);
            midDisplayAnim.SetBool("slingshotOn", false);
        }
    }

    private Vector3 CalculatePosition(float elapsedTime, Vector3 velocity, Vector3 initial_Pos)
    {
        return gravity * elapsedTime * elapsedTime * 0.5f +
                   velocity * elapsedTime + initial_Pos;
    }

    IEnumerator slingshotShake(float distance) {
        Tween myTween = vaccineholder.DOLocalMove(origin.localPosition, distance / velocitytemp.magnitude + .1f);
        yield return myTween.WaitForCompletion();
        Vector3 tempforward = velocitytemp;
        vaccineholder.DOShakePosition(tempforward.magnitude * .05f, tempforward * .02f, 4);
    }

    public void SlingShotActivated() {
        slingshotActivated = true;
    }

    public void SlingShotDeactivated()
    {
        slingshotActivated = false;
    }
}
