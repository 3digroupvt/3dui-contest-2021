﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransition : MonoBehaviour
{
    public int cameraView = 0;
    public float speed = 1;

    /*public bool firstPersonActive = true;*/
    public GameObject firstPersonView;
    public GameObject thirdPersonViewRight;
    public GameObject thirdPersonViewLeft;

    private GameObject[] povs;

    // Start is called before the first frame update
    void Start()
    {
        povs = new GameObject[] { firstPersonView, thirdPersonViewRight, thirdPersonViewLeft };
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {

            /*firstPersonActive = !firstPersonActive;*/
            cameraView = (cameraView + 1) % 3;

        }
    }

    public void moveToFirstPersonView()
    {
        cameraView = 0;
    }
    public void moveToThirdPersonViewRight()
    {
        cameraView = 1;
    }

    public void moveToThirdPersonViewLeft()
    {
        cameraView = 2;
    }

    private void LateUpdate()
    {

        Vector3 newPosition = Vector3.Lerp(transform.position, povs[cameraView].transform.position, Time.deltaTime * speed);
        Quaternion newRotation = Quaternion.Slerp(transform.rotation, povs[cameraView].transform.rotation, Time.deltaTime * speed);

        transform.position = newPosition;
        transform.rotation = newRotation;

        /* if (cameraView == 0)
        {

            Vector3 newPosition = Vector3.Lerp(transform.position, firstPersonView.transform.position, Time.deltaTime * speed);
            Quaternion newRotation = Quaternion.Slerp(transform.rotation, firstPersonView.transform.rotation, Time.deltaTime * speed);

            transform.position = newPosition;
            transform.rotation = newRotation;

        }
        else if (cameraView == 1){

            Vector3 newPosition = Vector3.Lerp(transform.position, thirdPersonViewRight.transform.position, Time.deltaTime * speed);
            Quaternion newRotation = Quaternion.Slerp(transform.rotation, thirdPersonViewRight.transform.rotation, Time.deltaTime * speed);

            transform.position = newPosition;
            transform.rotation = newRotation;

        }
        else
        {

            Vector3 newPosition = Vector3.Lerp(transform.position, thirdPersonViewLeft.transform.position, Time.deltaTime * speed);
            Quaternion newRotation = Quaternion.Slerp(transform.rotation, thirdPersonViewLeft.transform.rotation, Time.deltaTime * speed);

            transform.position = newPosition;
            transform.rotation = newRotation;

        }*/
    }
}
