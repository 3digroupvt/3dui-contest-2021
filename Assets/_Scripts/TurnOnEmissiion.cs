﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnEmissiion : MonoBehaviour
{
    
    [SerializeField]Renderer rend;

   
    // Start is called before the first frame update
    void Start()
    {

        rend = GetComponent<Renderer>();
    }

    

    public void EmissionOn()
    {
        rend.material.EnableKeyword("_EMISSION");
    }

    public void EmissionOff()
    {
        rend.material.DisableKeyword("_EMISSION");
    }
}
