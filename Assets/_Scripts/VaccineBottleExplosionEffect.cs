﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaccineBottleExplosionEffect : MonoBehaviour
{

    public bool startExplosion;
    ParticleSystem ps;
    public GameObject vaccineBottle;
    
    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (startExplosion)
        {
            Explode();
            startExplosion = false;
        }

       
    }

    void Explode()
    {
        ps.Play();
        Destroy(vaccineBottle);
        StartCoroutine(DestroyParticleSystem(3f));
    }

    public void StartExplosionEffect()
    {
        startExplosion = true;
    }

    IEnumerator DestroyParticleSystem(float t)
    {
        yield return new WaitForSeconds(t);
        Destroy(ps);
    }
}
