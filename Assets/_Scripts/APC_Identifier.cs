﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APC_Identifier : MonoBehaviour
{
    public bool antibody_attached;
    public bool antibody_attached_blue;
    public bool antibody_attached_green;
    public bool antibody_attached_yellow;
    public bool isIdentified;
    // Start is called before the first frame update
    void Start()
    {
        antibody_attached = false;
        antibody_attached_blue = false;
        antibody_attached_green = false;
        antibody_attached_yellow = false;
        isIdentified = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setBool(int color, bool value) {
        if (color == 1)
        {
            antibody_attached_blue = value;

        }
        else if (color == 2) {
            antibody_attached_green = value;

        }
        else if (color == 3)
        {
            antibody_attached_yellow = value;

        }
    }

    public void setBool(bool value)
    {
        antibody_attached = value;
    }
}
