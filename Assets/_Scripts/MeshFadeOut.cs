﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MeshFadeOut : MonoBehaviour
{

    [SerializeField] MeshRenderer[] rend;
    public MeshRenderer rend2;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponentsInChildren<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FadeOut()
    {
        foreach(MeshRenderer r in rend)
        {
            r.material.DOFade(0.0f, 5f);
        }
    }

    public void FadeOutTest()
    {
        rend2.GetComponent<MeshRenderer>().material.DOFade(0.0f, 3f);
    }
}
