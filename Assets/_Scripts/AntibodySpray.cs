﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AntibodySpray : MonoBehaviour
{
    public GameObject antibody;
    public GameObject scanner;
    public GameObject rangeNotice;
    public AudioSource errorAudio;
    public UserControl uc;

    public enum colorType { blue, green, yellow};
    public colorType _colorType;

    public List<ParticleCollisionEvent> collisionEvents;

    public bool isTimelineResumed;

    public static bool antibodyActivated;

    //public static bool scannerSpawned;

    public static float antibodyAmount = 100f;

    public GameObject aimingReticle;
    public Image progressBar;
    public Text progressAmount;
    private ParticleSystem part;
    private Vector3 lastpos;
    public AudioSource antibodyAudio;
    private bool antibodyexhausted = false;
    private Vector3 lastPosTemp;
    // Start is called before the first frame update
    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
        antibodyActivated = false;
        part.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.Get(OVRInput.RawButton.RIndexTrigger) && !SlingShot.slingshotActive && antibodyActivated && antibodyAmount > 0.1f)
        //if (Input.GetKey(KeyCode.Space))
        {
            if (!uc.cellinRange())
            {
                if (rangeNotice != null)
                {
                    if (!errorAudio.isPlaying && !rangeNotice.activeSelf)
                    {
                        errorAudio.Play();
                        StartCoroutine(displayRangeIndicator());
                    }
                }
            }
            else
            {
                //if (!part.isPlaying)
                //{
                part.Emit(1);
                if (antibodyAudio != null && !antibodyAudio.isPlaying)
                {
                    antibodyAudio.Play();
                }
                //}
                if (progressBar != null)
                {
                    if (antibodyAmount > 0.1f)
                    {
                        antibodyAmount -= Time.deltaTime * 2f;

                        progressBar.fillAmount = antibodyAmount / 100f;
                        progressAmount.text = Mathf.RoundToInt(antibodyAmount) + "";
                    }
                    else
                    {
                        progressBar.fillAmount = 0f;
                        progressAmount.text = "0";
                        part.Stop();
                        if (antibodyAudio != null && antibodyAudio.isPlaying)
                        {
                            antibodyAudio.Stop();
                        }
                    }
                }
                else
                {
                    if (antibodyAmount < 0.1f)
                    {
                        part.Stop();
                        if (antibodyAudio != null)
                        {
                            antibodyAudio.Stop();
                        }
                        if (!antibodyexhausted)
                        {
                            GameObject temp = Instantiate(scanner, lastpos, Quaternion.identity);
                            antibodyexhausted = true;
                        }
                    }

                }
            }
        }

        if (antibodyAmount < 0.1f)
        {
            if (antibodyAudio != null && antibodyAudio.isPlaying)
            {
                antibodyAudio.Stop();
            }
            if (!antibodyexhausted)
            {
                GameObject temp = Instantiate(scanner, lastpos, Quaternion.identity);
                antibodyexhausted = true;
            }
        }
        //Debug.Log(uc.cellinRange());

        if (OVRInput.GetUp(OVRInput.RawButton.RIndexTrigger) && !SlingShot.slingshotActive && antibodyActivated && antibodyAmount > 0.1f) 
        //if (Input.GetKeyUp(KeyCode.Space))
        {
            //if (part.isPlaying)
            //{
                //part.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                //part.Stop(true);
                if (antibodyAudio != null && antibodyAudio.isPlaying)
                {
                    antibodyAudio.Stop();

                    isTimelineResumed = true;
                }
            //}

            //if (!scannerSpawned)
            //{
            //    scannerSpawned = true;
            //}
            //temp.transform.SetParent(this.transform);
        }

        if (uc.cellinRange() && lastPosTemp != lastpos)
        {
            GameObject temp = Instantiate(scanner, lastpos, Quaternion.identity);
            lastPosTemp = lastpos;
        }
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.transform.parent.tag == "dendritic")
        {
            int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
            Vector3 pos = collisionEvents[0].intersection;
            Vector3 normal = collisionEvents[0].normal;
            //If not already there, instantiate the antibody at pos
            APC_Identifier temp = other.transform.parent.GetComponent<APC_Identifier>();
            if (temp != null && (!temp.antibody_attached_blue || !temp.antibody_attached_green || !temp.antibody_attached_yellow))
            {
                //scannerSpawned = false;
                GameObject antibodytemp = Instantiate(antibody, pos, Quaternion.identity);
                antibodytemp.transform.SetParent(other.transform.parent);
                antibodytemp.transform.forward = normal;
                antibodytemp.transform.GetChild(1).tag = other.transform.parent.tag;
                if (!temp.antibody_attached) {
                    temp.setBool(true);
                    lastpos = pos;
                }
                if (_colorType == colorType.blue)
                {
                    temp.setBool(1, true);
                }
                else if (_colorType == colorType.green) {
                    temp.setBool(2, true);
                }
                else if (_colorType == colorType.yellow)
                {
                    temp.setBool(3, true);
                }
            }

            //multiple antibody on one APC
            //if (_colorType == colorType.blue)
            //{
            //    if (temp != null && !temp.antibody_attached_blue)
            //    {
            //        lastpos = pos;
            //        GameObject antibodytemp = Instantiate(antibody, pos, Quaternion.identity);
            //        antibodytemp.transform.SetParent(other.transform.parent);
            //        antibodytemp.transform.forward = normal;
            //        temp.setBool(1, true);
            //    }
            //}
            //else if (_colorType == colorType.green) {
            //    if (temp != null && !temp.antibody_attached_green)
            //    {
            //        lastpos = pos;
            //        GameObject antibodytemp = Instantiate(antibody, pos, Quaternion.identity);
            //        antibodytemp.transform.SetParent(other.transform.parent);
            //        antibodytemp.transform.forward = normal;
            //        temp.setBool(2, true);
            //    }
            //}

        }
    }

    public void AntibodyActivate() {
        antibodyActivated = true;
        if (aimingReticle != null) {
            aimingReticle.SetActive(true);
        }
    }

    public void AntibodyDeactivate()
    {
        antibodyActivated = false;
        if (aimingReticle != null)
        {
            aimingReticle.SetActive(false);
        }
    }

    IEnumerator displayRangeIndicator() {
        rangeNotice.SetActive(true);
        yield return new WaitForSeconds(2f);
        rangeNotice.SetActive(false);
    }
}

