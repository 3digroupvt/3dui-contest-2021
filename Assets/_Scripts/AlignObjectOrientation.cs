﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignObjectOrientation : MonoBehaviour
{

    public Transform[] cellTransforms;
    float s;

    // Start is called before the first frame update
    void Start()
    {
        cellTransforms = GetComponentsInChildren<Transform>();

        foreach(Transform t in cellTransforms)
        {
            t.rotation = transform.rotation;
            s = Random.Range(0.4f, 1f);
            t.localScale = t.localScale * s;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
