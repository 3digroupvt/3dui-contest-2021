﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SpherecastCellDetection : MonoBehaviour
{

    public float maxDist;
    public PlayableDirector timelineDirector;

    public bool isTimelineResumed;
    public bool isCellGroupDetectorOn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isCellGroupDetectorOn)
        {
            ResumeAudioOnCloseCellDistance();
        }
    }

   
 /*   
    private void OnDrawGizmos()
    {
        float maxDist = 30f;
        RaycastHit hit;

        bool isHit = Physics.SphereCast(transform.position, transform.lossyScale.x*15, transform.forward, out hit, maxDist);

        if (isHit)
        {

            if(hit.collider.tag == "Cell Group")
            {
                Debug.Log("Cell group is hit");
                
                Gizmos.color = Color.green;
                Gizmos.DrawRay(transform.position, transform.forward * hit.distance);
                Gizmos.DrawWireSphere(transform.position + transform.forward * hit.distance, transform.lossyScale.x*15);

                

            }
            else
            {
                Gizmos.color = Color.red;
                Gizmos.DrawRay(transform.position, transform.forward * maxDist);
            }

        }
        


    }
 */

    void ResumeAudioOnCloseCellDistance()
    {
        
        RaycastHit hit;

        bool isHit = Physics.SphereCast(transform.position, transform.lossyScale.x*15, transform.forward, out hit, maxDist);


        if (isHit)
        {

            if (hit.collider.tag == "Cell Group")
            {
                Debug.Log("Cell group is close");

                if (timelineDirector.state == PlayState.Paused && !isTimelineResumed)
                {
                    isTimelineResumed = false;
                    timelineDirector.Resume();
                }


            }
            
        }

    }

    public void TurnOnCellDistanceDetector()
    {
        isCellGroupDetectorOn = true;
    }
}
