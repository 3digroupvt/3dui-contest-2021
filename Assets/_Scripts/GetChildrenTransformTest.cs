﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetChildrenTransformTest : MonoBehaviour
{

   
    public GameObject joints;
    public List<GameObject> dendriteHeads = new List<GameObject>();
    
    // Start is called before the first frame update
    void Start()
    {
        dendriteHeads = GetChildrenWithTag(joints);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public List<GameObject> GetChildrenWithTag(GameObject obj)
    {
        var childrenWithTag = new List<GameObject>();
        var allChildren = obj.GetComponentsInChildren<Transform>();
        foreach (var child in allChildren)
        {
            if (child.gameObject.CompareTag("Dendrite"))
            {
                childrenWithTag.Add(child.gameObject);
            }
        }
        return childrenWithTag;
    }

}
