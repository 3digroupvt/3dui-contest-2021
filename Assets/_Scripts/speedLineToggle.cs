﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speedLineToggle : MonoBehaviour
{
    private ParticleSystem ps;
    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    public void toggleOn() {
        ps.Play();
    }

    public void toggleOff()
    {
        ps.Stop();
    }
}
