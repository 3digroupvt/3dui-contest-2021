﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drawLine : MonoBehaviour
{
    private Transform[] dots;
    private LineRenderer lr;
    private int len;
    // Start is called before the first frame update
    void Start()
    {
        dots = transform.GetComponentsInChildren<Transform>();
        lr = GetComponent<LineRenderer>();
        len = dots.Length - 1;
        lr.positionCount = len;
        for (int i = 0; i < len; i++)
        {
            lr.SetPosition(i, dots[i + 1].localPosition);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
