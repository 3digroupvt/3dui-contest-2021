﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionalArrowAnimated : MonoBehaviour {

    public GameObject[] arrows;
    public bool showArrowDirection;
    public float frequency = .5f;
	// Use this for initialization
	void Start () {
        InvokeRepeating("ArrowAnimation", 1f, 1.6f);
    }

    private void Update()
    {
       
        if (showArrowDirection)
        {
            foreach (GameObject a in arrows)
            {
                a.SetActive(true);
            }
        }
        else
        {
            foreach(GameObject a in arrows)
            {
                a.SetActive(false);
            }
        }
    }


    void ArrowAnimation()
    {
        if (gameObject.activeSelf)
        {
            StartCoroutine(ArrowOnOff(frequency));
        }
    }


    IEnumerator ArrowOnOff(float second)
    {
        foreach (GameObject a in arrows)
        {
            a.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
            yield return new WaitForSeconds(second);
            a.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
        }
    }

/*
    IEnumerator ArrowOnOff2(float second)
    {
        foreach (GameObject a in arrows)
        {
            a.SetActive(true);
            yield return new WaitForSeconds(second);
            a.SetActive(false);
        }
    }
*/
}

