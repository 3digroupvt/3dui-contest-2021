﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkingLight : MonoBehaviour
{

    public GameObject lightSelected;

    Renderer rend;

    //Renderer rend;
    public bool isLightOn;
    public float minTime;
    public float maxTime;
    public float randomTime;
    Color32 orange = new Color32(248, 166, 11, 255);

    float lastTime;

    public bool isTalking = false;

    public bool isCaptainSpeaking;
    public bool isDrSpeaking;



    // Start is called before the first frame update
    void Start()
    {

        rend = lightSelected.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {

        randomTime = Random.Range(minTime, maxTime);

        if (Time.time - lastTime > randomTime)
        {
            lastTime = Time.time;
            /*
                        if (isCaptainSpeaking)
                        {
                            StartCoroutine(FlickeringLightGreen(randomTime));
                        }
                        else
                        {
                            isCaptainSpeaking = false;
                            StartCoroutine(FlickeringLightOrange(randomTime));
                        }

            */

            if (isCaptainSpeaking)
            {
                StartCoroutine(FlickeringLightGreen(randomTime));
            }

            if (isDrSpeaking)
            {
                StartCoroutine(FlickeringLightOrange(randomTime));
            }


        }




    }


    IEnumerator FlickeringLightOrange(float time)
    {
        yield return new WaitForSeconds(time);
        isLightOn = !isLightOn;

        if (isLightOn)
        {
            rend.material.EnableKeyword("_EMISSION");
            rend.material.SetColor("_EmissionColor", orange);

        }
        else
        {
            rend.material.DisableKeyword("_EMISSION");
        }



    }



    IEnumerator FlickeringLightGreen(float time)
    {
        yield return new WaitForSeconds(time);
        isLightOn = !isLightOn;

        if (isLightOn)
        {
            rend.material.EnableKeyword("_EMISSION");
            rend.material.SetColor("_EmissionColor", Color.green);
        }
        else
        {
            rend.material.DisableKeyword("_EMISSION");
        }

    }


    public void CaptainSpeaks()
    {
        isDrSpeaking = false;
        isCaptainSpeaking = true;
       
    }

    public void DrVaxSpeaks()
    {
        isCaptainSpeaking = false;
        isDrSpeaking = true;
    }

    public void NoOneSpeaks()
    {
        isCaptainSpeaking = false;
        isDrSpeaking = false;

        rend.material.DisableKeyword("_EMISSION");
    }
}
