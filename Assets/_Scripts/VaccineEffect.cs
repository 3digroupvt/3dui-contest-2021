﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaccineEffect : MonoBehaviour
{
    ParticleSystem ps;
    private GameObject temp;
    // Start is called before the first frame update
    void Start()
    {
        temp = Resources.Load("VaccineExplosion") as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent.tag == "dendritic" && other.GetComponent<morphingDendriticCell>() != null && !other.GetComponent<morphingDendriticCell>().startMorphing)
        {
            Instantiate(temp, gameObject.transform.position, Quaternion.identity);
            GameObject.FindGameObjectWithTag("Player").GetComponent<UserControl>().addLevel();
            Destroy(gameObject);
        }
    }
}
