﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using SWS;
using DG.Tweening;
using TMPro;
public class ManualSplinemoveControl : MonoBehaviour
{
    public PlayableDirector PD;
    public UserControl uc;
    public GameObject player;
    //public GameObject gunassembly;
    public Vector2 joystickR;
    public Vector2 joystickL;
    public Transform shipChild;
    public float sensitivity;
    public TextMeshPro drivingmode;
    public AudioSource backHint;

    splineMove spline;
    public float currentSpeed;
    public GameObject arrowL;
    public GameObject arrowR;
    private float timer = 0f;
    private bool hintPlayed = false;
    // Start is called before the first frame update
    void Start()
    {
        spline = GetComponent<splineMove>();
        currentSpeed = spline.speed;
    }

    // Update is called once per frame
    void Update()
    {
        joystickR = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
        joystickL = OVRInput.Get(OVRInput.RawAxis2D.LThumbstick);

        //forward
        if ((PD.time < 240 && spline.currentPoint < 13) || (PD.time > 270 && PD.time < 290 && spline.currentPoint < 16))
        {
            if (joystickL.y > 0.1f)
            {
                spline.ChangeSpeed(5f * joystickL.y);
                spline.Resume();
                hintPlayed = false;
                //timer += Time.deltaTime;
                //if (timer >= 2f && Mathf.Abs(shipChild.localRotation.eulerAngles.y) > 30f) {
                //    StartCoroutine(turnShip());
                //}
            }
        }
        else {
            if (!UserControl.taskCompleted && !hintPlayed) {
                backHint.Play();
                hintPlayed = true;
            }
            spline.Pause();
        }
        if (joystickL.y < -0.1f)
        {
            spline.ChangeSpeed(-5f + joystickL.y);
            spline.Resume();
            arrowL.SetActive(false);
            arrowR.SetActive(false);
            //timer = 0f;
        }
        else if (joystickL.y > -0.1f && joystickL.y < 0.1f)
        {
            spline.Pause();
            //timer = 0f;
            arrowL.SetActive(false);
            arrowR.SetActive(false);
        }


        //nanoship rotation
        //rotate
        //if (joystickL.x > 0.7f || joystickL.x < -0.7f)
        //{
        //    player.transform.Rotate(0, joystickL.x * sensitivity, 0);
        //}
        Vector3 tempRotation = shipChild.localRotation.eulerAngles;
        //shipChild.localRotation = Quaternion.Euler(tempRotation.x, tempRotation.y + joystickR.x * Time.deltaTime * sensitivity, tempRotation.z);

        Vector3 tempRotationGun = shipChild.transform.localRotation.eulerAngles;
        float tempRot = 0f;
        if (tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity < 30f)
        {
            tempRot = tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity;
        }
        else if (tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity > 330f)
        {
            tempRot = tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity;
        }
        else if (tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity >= 30f && tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity < 40f)
        {
            tempRot = 30f;
        }
        else if (tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity <= 330f && tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity > 320f)
        {
            tempRot = 330f;
        }

        shipChild.transform.localRotation = Quaternion.Euler(tempRot, tempRotation.y + joystickR.x * Time.deltaTime * sensitivity, tempRotationGun.z);

        //Vector3 tempRotationGun = gunassembly.transform.localRotation.eulerAngles;
        //float tempRot = 0f;
        //if (tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity < 10f) {
        //    tempRot = tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity;
        //}
        //else if (tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity > 350f)
        //{
        //    tempRot = tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity;
        //}
        //else if (tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity >= 10f && tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity < 20f)
        //{
        //    tempRot = 10f;
        //}
        //else if (tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity <= 350f && tempRotationGun.x - joystickR.y * Time.deltaTime * sensitivity > 340f)
        //{
        //    tempRot = 350f;
        //}

        //gunassembly.transform.localRotation = Quaternion.Euler(tempRot, tempRotationGun.y, tempRotationGun.z);

    }

    private void OnEnable()
    {
        uc.ShowController();
        drivingmode.text = "MANUAL-PILOTING";
    }

    private void OnDisable()
    {
        uc.HideController();
        drivingmode.text = "AUTO-PILOTING";
    }

    IEnumerator turnShip() {
        arrowL.SetActive(true);
        arrowR.SetActive(true);
        Tween mytween = shipChild.DOLocalRotate(Vector3.zero, 5f);
        yield return mytween.WaitForCompletion();
        arrowL.SetActive(false);
        arrowR.SetActive(false);
    }
}
