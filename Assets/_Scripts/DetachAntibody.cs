﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetachAntibody : MonoBehaviour
{
    private Transform[] antibodies;
    public bool isdetaching = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (isdetaching)
        {
            int count = transform.childCount;
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    transform.GetChild(i).SetParent(null);
                }
            }
        }
    }

    public void Detach() {
        isdetaching = true;
    }
}
