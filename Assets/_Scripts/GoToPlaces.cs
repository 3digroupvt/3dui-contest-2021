﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GoToPlaces : MonoBehaviour
{

    morphingDendriticCell morphingDCscript;
    public List<GameObject> destination = new List<GameObject>();
    public GameObject placeToMove;
    bool isPlaceAdded;
    bool isMoving;
    public float stopDistance;
    public float moveSpeed;
    public float waitTime = 15f;

    // Start is called before the first frame update
    void Start()
    {
        morphingDCscript = GetComponentInChildren<morphingDendriticCell>();
    }

    // Update is called once per frame
    void Update()
    {

        if (morphingDCscript.startMorphing && !isPlaceAdded)
        {
            StartCoroutine(MoveToPlaces(waitTime));
            
            
            isPlaceAdded = true;
        }

        if (isMoving)
        {
            //transform.DOLookAt(placeToMove.transform.position, 2f);
            transform.position = Vector3.MoveTowards(transform.position, placeToMove.transform.position, Time.deltaTime * moveSpeed);
            Quaternion r = Quaternion.LookRotation(placeToMove.transform.position - transform.position);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, r, 100f * Time.deltaTime);
        }

        if(placeToMove != null && Vector3.Distance (transform.position, placeToMove.transform.position) <= stopDistance)
        {
            moveSpeed = 0f;
            isMoving = false;
        }

    }

    IEnumerator MoveToPlaces(float t)
    {
        yield return new WaitForSeconds(t);

        GameObject[] allPlaces = GameObject.FindGameObjectsWithTag("Place to go");
        destination.AddRange(allPlaces);
        //Debug.Log(destination[0].name);

        placeToMove = destination[0];
        destination[0].tag = "Place to go void";
        isMoving = true;
    }
    

    
}
