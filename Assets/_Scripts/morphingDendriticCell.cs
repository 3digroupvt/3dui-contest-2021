﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class morphingDendriticCell : MonoBehaviour
{
    Animator anim;
    public bool startMorphing;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
       // if (Input.GetKeyDown("space"))
       // {
       //     anim.SetTrigger("Activation Morphing");
       // }

        if (startMorphing)
        {
            anim.SetTrigger("Activation Morphing");
        }
    }

    public void StartChangingShape()
    {
        startMorphing = true;
    }
}
