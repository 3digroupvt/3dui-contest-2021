﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/**
 * Simple orbit script which you can change the angle and offset of
 * the orbit
 * 
 * @author Luke Schlueter
 * @version 2021.3.1
 */

public class Orbit : MonoBehaviour
{
	public float xSpread = 100.0f;
	public float zSpread = 100.0f;
	public float ySpread = 100.0f;
    public float yOffset = 0.0f;
	public float angle;
	public Transform centerPoint;

	public float rotSpeed = 5.0f;
	public bool rotateClockwise;

    public float threshold;
    public ParticleSystem ps;

	float timer = 0;
	private float xVal;
	private float yVal;
	private float zVal;
    private float zinitial;
    private float randoffset;

    private void Start()
    {
        zinitial = transform.localPosition.z;
        randoffset = UnityEngine.Random.Range(0, 100);
    }

    // Update is called once per frame
    void Update(){
        timer += Time.deltaTime * rotSpeed;
        Rotate();
        transform.LookAt(centerPoint);
    }

    void Rotate(){
        angle = angle % 180;
		yVal = Math.Abs(angle / 90) * ySpread;
		xVal = Math.Abs((90 - angle) / 90) * xSpread;
    	if(rotateClockwise){
            //float x = -Mathf.Cos(timer) * xVal;
            //float z = Mathf.Sin(timer) * zSpread;
            //float y = Mathf.Cos(timer) * yVal;
            //Vector3 pos = new Vector3(x, y + yOffset, z);
            //transform.position = pos + centerPoint.position;

            //Some experiment trials
            float x = Mathf.Cos(timer + randoffset) * xSpread;
            float z = Mathf.Sin(timer + randoffset) * zSpread;
            float y = Mathf.Cos(timer) * yVal;
            Vector3 pos = new Vector3(x, z, yOffset + zinitial);
            transform.localPosition = pos;
        }
    	else{
    		float x = Mathf.Cos(timer) * xVal;
    		float z = Mathf.Sin(timer) * zSpread;
            float y = Mathf.Cos(timer) * yVal;
    		Vector3 pos = new Vector3(x, y + yOffset, z);
    		transform.position = pos + centerPoint.position;
    	}

        if (Vector3.Distance(transform.position, centerPoint.position) < threshold)
        {
            if (!ps.isPlaying)
            {
                ps.Play();
            }
        }
        else {
            if (ps.isPlaying)
            {
                ps.Stop();
            }
        }
    }

}
