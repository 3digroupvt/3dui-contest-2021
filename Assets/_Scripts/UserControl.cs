﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Playables;
using SWS;

public class UserControl : MonoBehaviour
{
    public float sensitivity = 2f;
    public splineMove sm;
    public Material controllerMat;
    public TextMeshPro driveMode;
    private Vector3 tempRotation;
    public bool isControllerActive;
    public GameObject autopilotIcon;
    public GameObject manualpilotIcon;
    public GameObject aimingReticle;
    public PlayableDirector PD;
    private Vector3 temoPos;
    private Vector3 tempRot;
    private bool iscloakon = false;
    private Vector3 lastPos;
    private bool isPaused1 = false;
    private bool isPaused2 = false;
    private bool isPaused3 = false;
    private bool isPaused4 = false;
    public Text speedUI_int;
    public Text speedUI_float;
    public Image speedCircle;
    public Image cloakCircle;
    public int crp_int = 2;
    public Text crpUI_int;
    public Image crpCircle;
    public float speed;
    public static bool taskCompleted = false;
    public GameObject[] shipbody;
    public GameObject shipchild;
    public GameObject[] controllerhint;
    public GameObject cloakonUI;
    public GameObject cloakoffUI;
    public GameObject batteryHigh;
    public GameObject batteryMed;
    public GameObject batteryLow;
    public GameObject BoidLymph;
    public GameObject BoidoutsideLymph;
    public GameObject OutsideEnv;
    public GameObject BCellAttackGroup;
    public GameObject APCLymph;
    public GameObject autoPilotIcon;
    public GameObject[] APCs;
    private Material originMat;
    public Material holoMat;
    public GameObject auxEngine;
    public SpriteRenderer creditInfo;
    // Start is called before the first frame update
    void Start()
    {
        //ShowController();
        isControllerActive = false;
        manualpilotIcon.SetActive(false);
        autopilotIcon.SetActive(true);
        originMat = shipbody[0].GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        //if (isControllerActive)
        //{
        //    var leftJoystick = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
        //    var rightJoystick = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);

        //    Vector3 offsetPos = new Vector3(leftJoystick.x, rightJoystick.y, leftJoystick.y);
        //    //transform.position += offsetPos * Time.deltaTime * sensitivity * 2f;
        //    transform.position += (offsetPos.z * transform.forward + offsetPos.x * transform.right + offsetPos.y * transform.up) * Time.deltaTime * sensitivity * 1f;

        //    tempRotation = transform.GetChild(1).localRotation.eulerAngles;
        //    transform.GetChild(1).localRotation = Quaternion.Euler(tempRotation.x, tempRotation.y + leftJoystick.x * Time.deltaTime * sensitivity, tempRotation.z);
        //}
        //Debug.Log(sm.currentPoint);
        speed = Vector3.Distance(transform.position, lastPos) / Time.deltaTime;
        speedUI_int.text = Mathf.RoundToInt(speed / 20f * 100f) + "";
        if (speed > 0)
        {
            speedUI_float.text = (speed / 20f * 100f - Mathf.RoundToInt(speed / 20f * 100f)).ToString().Substring(3, 2);
        }
        else {
            speedUI_float.text = "00";
        }
        speedCircle.fillAmount = Mathf.Round(speed) / 20f * .607f;

        //if (OVRInput.GetDown(OVRInput.RawButton.X))
        //{
        //    if (!isControllerActive)
        //    {
        //        ShowController();
        //        RecordPos();
        //        DOTween.Pause("pathing");
        //        driveMode.text = "Manual Piloting";
        //        manualpilotIcon.SetActive(true);
        //        autopilotIcon.SetActive(false);
        //    }
        //    else
        //    {
        //        HideController();
        //        StartCoroutine(backtoRoute());
        //        driveMode.text = "Auto Piloting";
        //        manualpilotIcon.SetActive(false);
        //        autopilotIcon.SetActive(true);

        //    }
        //}

        lastPos = transform.position;

        if (sm.currentPoint >= 10 && PD.state == PlayState.Paused && !isPaused1)
        {
            PD.Resume();
            isPaused1 = true;
        }

        if (sm.currentPoint >= 12 && PD.state == PlayState.Paused && !isPaused2 && isPaused1)
        {
            PD.Resume();
            isPaused2 = true;
        }

        if (ScannerSpawn.count >= 3 && PD.state == PlayState.Paused && !isPaused3 && isPaused1 && isPaused2) {
            PD.Resume();
            isPaused3 = true;
        }


        if (sm.currentPoint >= 15 && PD.state == PlayState.Paused && !isPaused4 && isPaused1 && isPaused2 && isPaused3)
        {
            PD.Resume();
            sm.Pause();
            gameObject.GetComponent<ManualSplinemoveControl>().enabled = true;
            ShowController();
            autoPilotIcon.SetActive(false);
            isPaused4 = true;
        }

        if (iscloakon) {
            cloakCircle.fillAmount -= Time.deltaTime * .008f;
        }
    }

    private void LateUpdate()
    {

    }

    public void ShowController() {
        controllerMat.DOFloat(0f, "Vector1_6DDEBFA4", 1f);
        isControllerActive = true;
        for (int i = 0; i < controllerhint.Length; i++)
        {
            controllerhint[i].SetActive(true);
        }
    }

    public void HideController()
    {
        controllerMat.DOFloat(1f, "Vector1_6DDEBFA4", 1f);
        isControllerActive = false;
        for (int i = 0; i < controllerhint.Length; i++)
        {
            controllerhint[i].SetActive(false);
        }
    }

    void RecordPos()
    {
        temoPos = transform.position;
        tempRot = transform.rotation.eulerAngles;
    }

    IEnumerator backtoRoute()
    {
        Tween mytween = transform.DOMove(temoPos, 3f);
        transform.DORotate(tempRot, 3f);
        yield return mytween.WaitForCompletion();
        DOTween.Play("pathing");
    }

    public void addLevel() {
        if (crp_int < 15)
        {
            if ((sm.currentPoint <= 13 && crp_int < 9) || (sm.currentPoint > 13 && sm.currentPoint <= 16 && crp_int < 15))
            {
                crp_int += 3;
                crpUI_int.text = crp_int + " mg / L";
                crpCircle.fillAmount = (float)crp_int / 15 * .48f;
            }
            if (crp_int <= 9 && !isPaused4 && isPaused1 && isPaused2 && isPaused3) {
                StartCoroutine(resumeAfterMorph());
                //PD.Resume();
            }

            if (crp_int == 15 && PD.state == PlayState.Paused) {
                PD.Resume();
            }
        }
    }

    public void CloakOn() {
        for (int i = 0; i < shipbody.Length; i++) {
            shipbody[i].GetComponent<Renderer>().material = holoMat;
        }
        cloakonUI.SetActive(true);
        cloakoffUI.SetActive(false);
        cloakCircle.enabled = true;
        iscloakon = true;
    }

    public void CloakOff()
    {
        for (int i = 0; i < shipbody.Length; i++)
        {
            shipbody[i].GetComponent<Renderer>().material = originMat;
        }
        cloakonUI.SetActive(false);
        cloakoffUI.SetActive(true);
        cloakCircle.enabled = false;
        iscloakon = false;
    }

    public void BatteryLow() {
        batteryHigh.SetActive(false);
        batteryMed.SetActive(false);
        batteryLow.SetActive(true);

    }

    public void enableLymphObjects() {
        BoidoutsideLymph.SetActive(false);
        OutsideEnv.SetActive(false);
        BoidLymph.SetActive(true);
        APCLymph.SetActive(true);
    }

    public void turnAhead() {
        shipchild.transform.DOLocalRotate(Vector3.zero, 3f);
    }

    public void startAuxEngine() {
        auxEngine.SetActive(true);
    }

    public void enableBcells() {
        BCellAttackGroup.SetActive(true);
    }

    IEnumerator resumeAfterMorph() {
        yield return new WaitForSeconds(8f);
        PD.Resume();
    }

    public void activateReticle() {
        aimingReticle.SetActive(true);
        manualpilotIcon.SetActive(true);
        autopilotIcon.SetActive(false);
    }

    public void deactivateReticle()
    {
        aimingReticle.SetActive(false);
    }

    public bool cellinRange() {

        if (sm.currentPoint < 13)
        {
            for (int i = 0; i < 3; i++)
            {
                if (Vector3.Distance(transform.position, APCs[i].transform.position) < 50f)
                {
                    return true;
                }
            }
            return false;
        }
        else if (sm.currentPoint > 14 && sm.currentPoint < 16)
        {
            for (int i = 3; i < 5; i++)
            {
                if (Vector3.Distance(transform.position, APCs[i].transform.position) < 50f)
                {
                    return true;
                }
            }
            return false;
        }
        else {
            return false;
        }

    }

    public void showCredit() {
        Color a = new Color(1f, 1f, 1f, 1f);
        creditInfo.DOColor(a, 1f);
    }

    public void setCompleteStatus(bool value)
    {
        taskCompleted = value;
    }

}
