﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInstantTransition : MonoBehaviour
{
    public GameObject thirdPersonPOV;
    public bool changeView;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (changeView)
        {
            CameraTransition();
            changeView = false;
        }

    }

    void CameraTransition()
    {
        transform.parent = null;
        transform.position = thirdPersonPOV.transform.position;
        transform.rotation = thirdPersonPOV.transform.rotation;
    }

    public void ThirdPersonPOV()
    {
        changeView = true;
    }
}
