﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LookatCam : MonoBehaviour
{
    private Transform nanobot;
    private Vector3 initialSCale;
    private bool labelUp;
    private bool alignCoord;
    private void OnEnable()
    {
        labelUp = true;
        alignCoord = false;
        nanobot = GameObject.FindGameObjectWithTag("Player").transform;
        initialSCale = transform.localScale;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.forward = nanobot.transform.forward;
        if (!alignCoord)
        {
            transform.up = transform.position - transform.parent.position;
            alignCoord = true;
        }
        if (OVRInput.GetDown(OVRInput.RawButton.A)) {
            labelUp = !labelUp;
            if (!labelUp)
            {
                transform.DOScale(Vector3.zero, .5f);
            }
            else {
                transform.DOScale(initialSCale, .5f);
            }
        }
    }
}
