﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class Turn2User : MonoBehaviour
{
    private GameObject player;
        
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, player.transform.position) < 50f)
        {
            transform.DOLookAt(player.transform.position, 2f);
           
        }
        
    }
}
