﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class TCellBinding : MonoBehaviour
{

    public GameObject joints;
    ParticleSystem psChemokine;
    //GameObject[] dendriteHeads;
    //Transform[] dendriteHeadsTrans;
    public List<GameObject> dendrites = new List<GameObject>();
    
    public List<GameObject> tcells = new List<GameObject>();
    List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();
    public bool startBinding1;
    public bool startBinding2;
    public bool startBinding3;
    public bool startBinding4;
    public bool startBinding5;
    public bool startBinding6;
    public bool startBinding7;
    public bool startBinding8;
    //public float moveSpeed;
    float stopSpeed = 0f;

    // Start is called before the first frame update
    void Start()
    {
   
        
        psChemokine = GetComponent<ParticleSystem>();
        dendrites = GetChildrenWithTag(joints);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (tcells.Count == 1)
        {
            startBinding1 = true;
        }

        if (startBinding1)
        {
            MoveToDendriticCell(tcells[0], dendrites[0], 0.4f);
        }

        if (tcells.Count == 2)
        {
            startBinding2 = true;
        }

        if (startBinding2)
        {
            MoveToDendriticCell(tcells[1], dendrites[1], 0.1f);
        }

        if (tcells.Count == 3)
        {
            startBinding3 = true;
        }

        if (startBinding3)
        {
            MoveToDendriticCell(tcells[2], dendrites[2], 0.2f);
        }

        if (tcells.Count == 4)
        {
            startBinding4 = true;
        }

        if (startBinding4)
        {
            MoveToDendriticCell(tcells[3], dendrites[3], 0.4f);
        }

        if (tcells.Count == 5)
        {
            startBinding5 = true;
        }

        if (startBinding5)
        {
            MoveToDendriticCell(tcells[4], dendrites[4], 0.3f);
        }

        if (tcells.Count == 6)
        {
            startBinding6 = true;
        }

        if (startBinding6)
        {
            MoveToDendriticCell(tcells[5], dendrites[5], 0.5f);
        }

        if (tcells.Count == 7)
        {
            startBinding7 = true;
        }

        if (startBinding7)
        {
            MoveToDendriticCell(tcells[6], dendrites[6], 0.3f);
        }

        if (tcells.Count == 8)
        {
            startBinding8 = true;
        }

        if (startBinding8)
        {
            MoveToDendriticCell(tcells[7], dendrites[7], 0.2f);
        }

    }

    private void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(psChemokine, other, collisionEvents);

        for (int i = 0; i < collisionEvents.Count; i++)
        {
                      
            if (other.transform.tag == "tcell" && tcells.Count <8)
            {

                tcells.Add(other.gameObject);
                tcells = tcells.Distinct().ToList();
                other.transform.tag = "tcell taken";

            }
        }
    }


    public List<GameObject> GetChildrenWithTag(GameObject obj)
    {
        var childrenWithTag = new List<GameObject>();
        var allChildren = obj.GetComponentsInChildren<Transform>();
        foreach (var child in allChildren)
        {
            if (child.gameObject.CompareTag("Dendrite"))
            {
                childrenWithTag.Add(child.gameObject);
            }
        }
        return childrenWithTag;
    }


    void MoveToDendriticCell(GameObject tCell, GameObject dendriteHead, float speed)
    {
        if(tCell != null)
        {
            tCell.transform.position = Vector3.MoveTowards(tCell.transform.position, dendriteHead.transform.position, Time.deltaTime * speed);
            Quaternion r = Quaternion.LookRotation(dendriteHead.transform.position - tCell.transform.position);
            tCell.transform.rotation = Quaternion.RotateTowards(tCell.transform.rotation, r, 100f * Time.deltaTime);

            if (Vector3.Distance(tCell.transform.position, dendriteHead.transform.position) < 1.5f)
            {
                TargetMover[] moverscripts = tCell.GetComponents<TargetMover>();

                foreach (TargetMover m in moverscripts)
                {
                    m.enabled = false;
                }


                tCell.transform.parent = dendriteHead.transform;
                //tCell.transform.localRotation = dendriteHead.transform.localRotation;
                speed = stopSpeed;
            }


        }



    }
}
